
```
#!bash

export T555DIR=$HOME/dev/t555
export T555TOOLS=$T555DIR/mkbootimg_tools
export T555GCC=$T555DIR/gcc-arm-eabi-4.8
export T555KERNELDIR=$T555DIR/kernel-lp
export T555KERNELOUT=$T555KERNELDIR/out
export T555BOOTDIRORIG=$T555DIR/boot-lp
export T555BOOTORIG=$T555BOOTDIRORIG/boot.img
export T555BOOTDIR=$T555TOOLS/boot_

if dpkg-query -W liblz4-tool; then
  # nothing
else
  sudo apt-get install liblz4-tool
fi

mkdir -p $T555DIR
cd $T555DIR
git clone --branch "master" --progress -v "https://github.com/xiaolu/mkbootimg_tools.git" $(basename $T555TOOLS)
git clone --branch "T555XXU1AOH2" --progress -v "https://bitbucket.org/smkernel/boot-sm-t555.git" $(basename $T555BOOTDIRORIG)
git clone --branch "android-5.1.1_r37" --progress -v "https://android.googlesource.com/platform/prebuilts/gcc/linux-x86/arm/arm-eabi-4.8" $(basename $T555GCC)
git clone --branch "lp-3" --progress -v "https://bitbucket.org/smkernel/android_kernel_samsung_gt510-lp.git" $(basename $T555KERNELDIR)

cd $T555TOOLS
rm -rf $T555BOOTDIR
cp -f $T555BOOTORIG $T555TOOLS/boot_orig.img
./mkboot boot_orig.img $(basename $T555BOOTDIR)

cd $T555BOOTDIR/ramdisk
sed -i "s/ro.secure=1/ro.secure=0/" ./default.prop
sed -i "s/ro.debuggable=0/ro.debuggable=1/" ./default.prop
sed -i "s/ro.adb.secure=1/ro.adb.secure=0/" ./default.prop
sed -i "s/persist.sys.usb.config=mtp/persist.sys.usb.config=adb,mtp/" ./default.prop
echo "ro.securestorage.support=false" >> ./default.prop
sed -i "s/wait,verify/wait/" ./fstab.qcom
sed -i "s/wait,check,encryptable=footer/wait,check/" ./fstab.qcom

cd $T555KERNELDIR
chmod 775 ./build_kernel.sh
chmod 775 ./mk_555.sh

cd $T555KERNELDIR
rm -rf $T555KERNELOUT

cd $T555KERNELDIR
export OUT_DIR=$(basename $T555KERNELOUT)

export CROSS_COMPILE=$T555GCC/bin/arm-eabi-
./mk_555.sh

cp -f $T555KERNELOUT/arch/arm/boot/dt.img $T555BOOTDIR/dt.img
cp -f $T555KERNELOUT/arch/arm/boot/zImage $T555BOOTDIR/kernel

cd $T555TOOLS
./mkboot $(basename $T555BOOTDIR) boot.img
echo -n "SEANDROIDENFORCE" >> boot.img

```